package mision.tic.backendProyecto.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Column;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioEntrada {
    @NonNull
    private String nombre;
    @NonNull
    private String correo;
    @NonNull
    private String contraseña;
    @NonNull
    private String contraseñaRep;

}
