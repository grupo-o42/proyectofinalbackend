package mision.tic.backendProyecto.repositorio;

import mision.tic.backendProyecto.modelo.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuariosRepositorio extends JpaRepository<Usuarios,Integer> {
    boolean existsByCorreo(String correo);
    Usuarios findByCorreo(String correo);
}
