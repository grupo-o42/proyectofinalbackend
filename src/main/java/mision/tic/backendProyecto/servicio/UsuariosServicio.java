package mision.tic.backendProyecto.servicio;

import mision.tic.backendProyecto.input.UsuarioEntrada;
import mision.tic.backendProyecto.modelo.Usuarios;
import mision.tic.backendProyecto.repositorio.UsuariosRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuariosServicio {
    @Autowired
    UsuariosRepositorio usuariosRepositorio;

    public boolean iniciarsesion(String correo, String contraseña) {
        if(usuariosRepositorio.existsByCorreo(correo)){
            Usuarios usuario = usuariosRepositorio.findByCorreo(correo);
            if(usuario.getContraseña().equals(contraseña)){
                return true;
            }else {
                return false;
            }
        }else{
            return false;
        }
    }

    public boolean agregarusuario(UsuarioEntrada usuarioEntrada) {
        try{
            if(usuarioEntrada.getContraseña().equals(usuarioEntrada.getContraseñaRep())){
                Usuarios usuario = new Usuarios(
                        usuarioEntrada.getNombre(),
                        usuarioEntrada.getCorreo(),
                        usuarioEntrada.getContraseña()
                );
                usuariosRepositorio.save(usuario);
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }
}
