package mision.tic.backendProyecto.controlador;

import mision.tic.backendProyecto.modelo.Generos;
import mision.tic.backendProyecto.servicio.GenerosServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class GenerosConstructor {

    @Autowired
    GenerosServicio generosServicio;

    @GetMapping("obtenerGeneros")
    public List<Generos> obtenerGeneros(){
        return generosServicio.obtenerGeneros();
    }

    @GetMapping("obtenerGeneroId")
    public Optional<Generos> obtenerGeneroId(@RequestParam int id){
        return generosServicio.obtenerGeneroId(id);
    }

    @PostMapping("crearGenero")
    public boolean crearGenero(@RequestBody Generos genero){
        return generosServicio.crearGenero(genero);
    }

}
