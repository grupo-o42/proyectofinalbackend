package mision.tic.backendProyecto.controlador;

import mision.tic.backendProyecto.input.UsuarioEntrada;
import mision.tic.backendProyecto.modelo.Usuarios;
import mision.tic.backendProyecto.servicio.UsuariosServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class UsuariosControlador {

    @Autowired
    UsuariosServicio usuariosServicio;


    @GetMapping("/iniciarsesion")
    public boolean iniciarsesion(@RequestParam String correo, @RequestParam String contraseña){
        return usuariosServicio.iniciarsesion(correo,contraseña);
    }

    @PostMapping("/agregarusuario")
    public boolean agregarusuario(@RequestBody UsuarioEntrada usuarioEntrada){
        return usuariosServicio.agregarusuario(usuarioEntrada);
    }

    @GetMapping("/obtenerUsuario")
    public boolean obtenerUsuario(@RequestParam String correo,@RequestParam String contraseña){
        return true;
    }





    @DeleteMapping("/eliminarusuario")
    public void eliminarusuario(){

    }
}
