package mision.tic.backendProyecto.controlador;

import mision.tic.backendProyecto.modelo.Canciones;
import mision.tic.backendProyecto.servicio.CancionesServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CancionesConstructor {

    @Autowired
    CancionesServicio cancionesServicio;

    @GetMapping("obtenerCanciones")
    public List<Canciones> obtenerCanciones(){
        return cancionesServicio.obtenerCanciones();
    }

    @PostMapping("agregarCanciones")
    public boolean agregarCanciones(@RequestParam int idGenero,@RequestBody Canciones cancion){
        return cancionesServicio.agregarCanciones(idGenero,cancion);
    }


}
